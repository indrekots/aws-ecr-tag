#!/usr/bin/env bats
setup() {
  DOCKER_IMAGE=${DOCKER_IMAGE:="indrekots/aws-ecr-tag"}

  echo "Building image..."
  docker build -t ${DOCKER_IMAGE}:test .
}

@test "AWS credentials are required" {
    run docker run \
        ${DOCKER_IMAGE}:test

    echo "Status: $status"
    echo "Output: $output"

    [ "$status" -eq 1 ]
}

@test "AWS_ACCESS_KEY_ID environment variable is required" {
    run docker run \
        -e AWS_SECRET_ACCESS_KEY=123 \
        ${DOCKER_IMAGE}:test

    echo "Status: $status"
    echo "Output: $output"

    [ "$status" -eq 1 ]
}

@test "AWS_SECRET_ACCESS_KEY environment variable is required" {
    run docker run \
        -e AWS_ACCESS_KEY_ID=abc \
        ${DOCKER_IMAGE}:test

    echo "Status: $status"
    echo "Output: $output"

    [ "$status" -eq 1 ]
}

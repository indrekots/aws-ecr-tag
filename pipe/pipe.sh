#!/usr/bin/env bash
#
# Allows to tag AWS ECR image
#

source "$(dirname "$0")/common.sh"

info "Executing the pipe..."

if [[ -z "${AWS_ACCESS_KEY_ID}" || -z "${AWS_SECRET_ACCESS_KEY}" ]]; then
  fail "AWS credentials not set. Make sure you have configured AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY environment variables."
fi

# Required parameters
REPOSITORY_NAME=${REPOSITORY_NAME:?'REPOSITORY_NAME variable missing.'}
IMAGE_TO_TAG=${IMAGE_TO_TAG:?'IMAGE_TO_TAG variable missing.'}
AWS_DEFAULT_REGION=${AWS_DEFAULT_REGION:?'AWS_DEFAULT_REGION variable missing.'}
NEW_TAG=${NEW_TAG:?'NEW_TAG variable missing.'}

# Default parameters
DEBUG=${DEBUG:="false"}

# Testing whether the image already has the tag we want to add
TAG_INDEX=$(aws ecr describe-images --repository-name ${REPOSITORY_NAME} --region ${AWS_DEFAULT_REGION} --image-ids imageTag=${IMAGE_TO_TAG} | jq ".imageDetails[0].imageTags | index(\"${NEW_TAG}\")")
if [[ ${TAG_INDEX} == "null" ]]; then
  info "Tag '$NEW_TAG' does not exist on the image, proceeding with re-tagging."
  info "Batch get image with tag $IMAGE_TO_TAG from repository $REPOSITORY_NAME in region $AWS_DEFAULT_REGION"
  MANIFEST=$(aws ecr batch-get-image --repository-name "$REPOSITORY_NAME" --image-ids imageTag="$IMAGE_TO_TAG" --query 'images[].imageManifest' --output text --region="$AWS_DEFAULT_REGION")
  info "Re-tagging the image..."
  aws ecr put-image --repository-name "$REPOSITORY_NAME" --image-tag "$NEW_TAG" --image-manifest "$MANIFEST" --region="$AWS_DEFAULT_REGION"
  status=0
else
  info "Tag '$NEW_TAG' already exists on the image. No need to re-tag."
  status=0
fi

if [[ "${status}" == "0" ]]; then
  success "Success!"
else
  fail "Error!"
fi
